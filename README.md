# Mac版本Java8下载

## 资源文件介绍

本仓库提供了一个资源文件的下载，该资源文件为Mac版本的Java 8 JDK安装包，具体文件名为`jdk-8u391-macosx-x64.dmg`，并已打包为zip格式。

## 文件说明

- **文件名**: `jdk-8u391-macosx-x64.dmg.zip`
- **版本**: Java 8 Update 391
- **适用系统**: macOS
- **文件类型**: DMG安装包（已打包为zip格式）

## 使用方法

1. 下载本仓库中的`jdk-8u391-macosx-x64.dmg.zip`文件。
2. 解压zip文件，得到`jdk-8u391-macosx-x64.dmg`。
3. 双击`jdk-8u391-macosx-x64.dmg`文件，按照提示完成Java 8的安装。

## 注意事项

- 请确保您的Mac系统符合Java 8的最低系统要求。
- 安装过程中请遵循系统提示，确保安装顺利完成。

## 其他说明

本资源文件仅供学习和开发使用，请勿用于商业用途。